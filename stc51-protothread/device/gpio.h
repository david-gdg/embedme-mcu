/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __GPIO_H__
#define __GPIO_H__
/**********************************************************
 File   : gpio.h
 Bref   : GPIO接口头文件 
 Version: V1.0.0
 Author : FergusZeng
 Date   : 2013-12-08
***********************************************************/

#include "basetype.h"
#include "board.h"
/* GPIO常用宏定义 */
#define GPIO_HIGH   1
#define GPIO_LOW    0

#define GPIO_MODE_IN_OUT    0x00//通用输入输出
#define GPIO_MODE_OUT_PP    0x01//推挽输出
#define GPIO_MODE_IN_HR     0x02//高阻输入
#define GPIO_MODE_OUT_OD    0x03//开漏

/*----------------------------------------------
 *在C51中GPIO定义为sbit类型,不可作为函数参数，
 *所以这里操作GPIO的函数定义为宏,而不是函数。
 ----------------------------------------------*/
#define GPIO_INIT(gpio,mode)	{;}
#define GPIO_GET_VAL(gpio)      (gpio)
#define GPIO_SET_VAL(gpio,val)  {(gpio)=!!(val);}

#if MCU_STC12LE5608
/* STC12LE5608单片机设置GPIO模式 */
#define PIN_0    	0
#define PIN_1       1
#define PIN_2       2
#define PIN_3       3
#define PIN_4       4
#define PIN_5       5
#define PIN_6       6
#define PIN_7       7
#define PIN_8       8
#define PIN_9       9
#define PIN_10      10
#define PIN_11      11
#define PIN_12      12
#define PIN_13      13
#define PIN_14      14
#define PIN_15      15
sfr P1M0=0x91;
sfr P1M1=0x92;
sfr P0M0=0x93;
sfr P0M1=0x94;
sfr P2M0=0x95;
sfr P2M1=0x96;
void gpio_set_mode_p0(uint8 pin,uint8 mode);
void gpio_set_mode_p1(uint8 pin,uint8 mode);
#endif

#endif

