/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "lcd.h"
#include "gpio.h"
#include "board.h"
#include "trace.h"
#include <stdio.h>

#define LCD_WIDTH   (16)
#define LCD_HEIGHT  (2)

/* 定义LCD控制线 */
#define LCD_RS          GPIO_DB1
#define LCD_RW          GPIO_DB2
#define LCD_EN          GPIO_LCDEN
#define LCD_DATA        P0
#define LCD_BUSY_BIT    (P0 & 0x80)

static void lcd_write_data(uint8 wdata)
{
    LCD_DATA=wdata;    
}


static bool lcd_is_busy(void) 
{ 
    bool ret;
    GPIO_SET_VAL(LCD_RS,0);
    GPIO_SET_VAL(LCD_RW,1);
    udelay(2);
    GPIO_SET_VAL(LCD_EN,1);
    ret = !!(LCD_BUSY_BIT);
    GPIO_SET_VAL(LCD_EN,0);
    return ret;

}
static void lcd_send_cmd(uint8 cmd)
{
    while(lcd_is_busy());
	GPIO_SET_VAL(LCD_RS,0);
	GPIO_SET_VAL(LCD_RW,0);
	GPIO_SET_VAL(LCD_EN,0);
	udelay(2);
	lcd_write_data(cmd);
	udelay(4);
	GPIO_SET_VAL(LCD_EN,1);
	udelay(4);
	GPIO_SET_VAL(LCD_EN,0);	
}

static void lcd_send_data(uint8 wdata)
{
    while(lcd_is_busy());
	GPIO_SET_VAL(LCD_RS,1);
	GPIO_SET_VAL(LCD_RW,0);
	GPIO_SET_VAL(LCD_EN,0);
	lcd_write_data(wdata);
	udelay(4);
	GPIO_SET_VAL(LCD_EN,1);
	udelay(4);
	GPIO_SET_VAL(LCD_EN,0);	
}

void lcd_init(void)
{
    GPIO_SET_VAL(LCD_RW,0);     /* 只写数据 */
    mdelay(15);   
    lcd_send_cmd(0x01);     /* 清除LCD的显示内容 */         
    lcd_send_cmd(0x38);     /* 16*2显示，5*7点阵，8位数据 */
    mdelay(5);
    lcd_send_cmd(0x38);         
    mdelay(5);
    lcd_send_cmd(0x38);         
    mdelay(5);

    lcd_send_cmd(0x0c);      /* 开显示，不显示光标 */ 
    mdelay(5);

    lcd_send_cmd(0x01);      /* 清除LCD的显示内容 */
    mdelay(5);
}

void lcd_clear(void)
{
    lcd_send_cmd(0x01); 
    mdelay(5);
}

void lcd_draw_chr(uint8 chr, uint8 x, uint8 y)
{
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    
    if(0==y)
    {
        lcd_send_cmd(0x80+x);
    }
    else
    {
        lcd_send_cmd(0x80+0x40+x);
    }
    lcd_send_data(chr);
}
void lcd_draw_wchr(uint16 wchr, uint8 x, uint8 y)
{
    uint8 chr;
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    chr = (wchr>>8)&0xFF;
    lcd_draw_chr(chr, x, y);
    chr = wchr & 0xFF;
    lcd_draw_chr(chr, x+1, y);
}
void lcd_draw_text(uint8 x, uint8 y, const char* text)
{
    uint8 i=0;
    uint16 wchr;
    if ((x>=LCD_WIDTH) ||
        (y>=LCD_HEIGHT))
    {
        return;
    }
    while(text[i]) 
 	{
 	    if(text[i]>0x80 && x<LCD_WIDTH)
 	    {
            wchr = text[i];
            wchr = (wchr<<8) | text[i+1];
            lcd_draw_wchr(wchr, x, y);
            i+=2;
            x+=2;
 	    }
 	    else
 	    {
            lcd_draw_chr(text[i++],x++,y);
        }
 	}
}
void lcd_draw_cursor(uint8 x, uint8 y)
{
    if ((x>=LCD_WIDTH) || (y>=LCD_HEIGHT))
    {
        return;
    }
}

