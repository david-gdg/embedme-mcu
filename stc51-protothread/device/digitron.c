/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "digitron.h"
#include "gpio.h"
#include "trace.h"
#include "board.h"

#define MAX_DIGITAL       16
#define MAX_POSITION      6
#define DIGITRON_SEGLCK   GPIO_DU       /* 段选 */
#define DIGITRON_POSLCK   GPIO_WE       /* 位选 */

/**************************
     __      A
    |__|    FGB
    |__|.   EDCH

数字1,点亮BC段  HGFEDCBA
共阴码为0x06    00000110
**************************/
#define DIGITRON_DATA     P0            /* 数据口 */
uint8 code table_digital[MAX_DIGITAL]={
0x3f,0x06,0x5b,0x4f,
0x66,0x6d,0x7d,0x07,
0x7f,0x6f,0x77,0x7c,
0x39,0x5e,0x79,0x71};

uint8 code table_pos[MAX_POSITION]={
0xfe,0xfd,0xfb,0xf7,0xef,0xdf};

void digitron_display(uint8 pos, uint8 digital)
{         
    if (pos<MAX_POSITION && digital<MAX_DIGITAL)
    {   
        GPIO_SET_VAL(DIGITRON_SEGLCK,0);
        DIGITRON_DATA = table_digital[digital];
        GPIO_SET_VAL(DIGITRON_SEGLCK,1);
        GPIO_SET_VAL(DIGITRON_SEGLCK,0);
    
        GPIO_SET_VAL(DIGITRON_POSLCK,0);
        DIGITRON_DATA = table_pos[pos];
        GPIO_SET_VAL(DIGITRON_POSLCK,1);
        GPIO_SET_VAL(DIGITRON_POSLCK,0);
        mdelay(5);
    }
}

