/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
/******************************************************************************
* @file     :   mytimer.h
* @bref     :   software timer header file
* @version  :   v.1.0
* @date     :   2013-12-12
* @author   :   FergusZeng
* @note     :   软件定时器
                最多可创建128个定时器(可根据mcu资源进行配置)
                最小精度为100ms
                最长定时为3276800ms
******************************************************************************/
#ifndef __MY_TIMER_H__
#define __MY_TIMER_H__

#include "basetype.h"

typedef enum{
    TIMER_TYPE_ONESHOT=0,
    TIMER_TYPE_PERIODIC,
}TIMER_TYPE_E;

typedef void (*SEL_ontimer)(void);

sint8 timer_create(uint8 timerType,uint16 ms,SEL_ontimer ontimer);
void timer_destroy(sint8 timerID);
bool timer_start(sint8 timerID);
bool timer_stop(sint8 timerID);
#endif
