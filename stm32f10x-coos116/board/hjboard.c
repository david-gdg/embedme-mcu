/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "board.h"
#include "hjboard.h"
#include "gpio.h"
#include "led.h"

static GpioInfo_S s_myBoardGpios[GPIO_MAX]={
    {P0_0, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 0: GPIO_LCD_D0 */
    {P0_1, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 1: GPIO_LCD_D1 */
    {P0_2, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 2: GPIO_LCD_D2 */
    {P0_3, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 3: GPIO_LCD_D3 */
    {P0_4, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 4: GPIO_LCD_D4 */
    {P0_5, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 5: GPIO_LCD_D5 */
    {P0_6, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 1},  /* 6: GPIO_LCD_D6 */
    {P0_7, GPIO_MODE_OUT_OD, GPIO_SPEED_10MHZ, 1},  /* 7: GPIO_LCD_D7:双向IO口,读BUSY信号 */
    {P1_0, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 8: GPIO_LCD_RS */
    {P1_1, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 9: GPIO_LCD_RW */
    {P2_5, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 10: GPIO_LCD_EN */
    {P1_0, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 11: GPIO_LED_L0 */
    {P1_1, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 12: GPIO_LED_L1 */
    {P1_2, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 13: GPIO_LED_L2 */
    {P1_3, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 14: GPIO_LED_L3 */
    {P1_4, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 15: GPIO_LED_L4 */
    {P1_5, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 16: GPIO_LED_L5 */
    {P1_6, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 17: GPIO_LED_L6 */
    {P1_7, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 18: GPIO_LED_L7 */
    {P2_7, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0},  /* 19: GPIO_DIG_DU */
    {P2_7, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0}   /* 20: GPIO_DIG_WE */
};

static void board_gpio_set(uint8 gpioID, bool enable)
{
    GpioInfo_S* gpios = s_myBoardGpios;
    if(gpioID<GPIO_MAX)
    {
        GPIO_SET_VAL(gpios[gpioID].m_gpio,(enable)?(gpios[gpioID].m_enVal):(!gpios[gpioID].m_enVal));
    }
}

static sint8 board_gpio_get(uint8 gpioID)
{
    GpioInfo_S* gpios = s_myBoardGpios;
    if(gpioID<GPIO_MAX)
    {
        return GPIO_GET_VAL(gpios[gpioID].m_gpio);
    }
    return -1;
}

static void board_gpio_init(void)
{
    /* 由于PB3,PB4,PA13,PA14,PA15这五个IO口默认是JTAG引脚,而我们只将其当做普通IO
     * 使用,所以需要改变管脚的映射，以禁用JTAG功能,否则没法控制这些IO口.
     * GPIO_Remap_SWJ_Disable:完全禁用JTAG_DP和SW_DP
     * GPIO_Remap_SWJ_JTAGDisable:JTAG_DP禁用,SW_DP使能
     */
    uint8 i;
    GpioInfo_S* gpios = s_myBoardGpios;
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_Disable, ENABLE);
    for(i=0; i<GPIO_MAX; i++)
    {
        GPIO_INIT(gpios[i].m_gpio,gpios[i].m_mode,gpios[i].m_speed);
    }
}

static void board_led_init(void)
{
    #if 1
    GpioInfo_S d1 = {STM32_LED_D1, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0};
    GpioInfo_S d2 = {STM32_LED_D2, GPIO_MODE_OUT_PP, GPIO_SPEED_10MHZ, 0};
    GPIO_INIT(d1.m_gpio,d1.m_mode,d1.m_speed);
    GPIO_INIT(d2.m_gpio,d2.m_mode,d2.m_speed);
    led_init(LED_L0, d1);
    led_init(LED_L1, d2);
    led_set_state(LED_L0, LED_STATE_BLINK_1HZ);
    led_set_state(LED_L1, LED_STATE_BLINK_2HZ);
    #endif
}
void board_init(void)
{    
    board_gpio_init();
    board_led_init();
}