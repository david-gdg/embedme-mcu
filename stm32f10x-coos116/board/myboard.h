/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MY_BOARD_H__
#define __MY_BOARD_H__

#include "basetype.h"
#include "uart.h"
#include "gpio.h"

/* 串口引脚配置 */
#define GPIO_UART1_TX   PA(9)
#define GPIO_UART1_RX   PA(10)
#define GPIO_UART2_TX   PA(2)
#define GPIO_UART2_RX   PA(3)
#define GPIO_UART3_TX   PB(10)
#define GPIO_UART3_RX   PB(11)

#define UART_MAX        2           /* 最大允许定义的串口数 */
#define COM_DEBUG       UART_COM0   /* 连接调试串口 */
#define COM_DEBUG_BAUD  115200

typedef enum{
    GPIO_KEY_K1=0,
    GPIO_KEY_K2,
    GPIO_MAX
}GPIO_ID_E;

#endif
