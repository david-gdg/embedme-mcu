/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "rtc.h"
#include "trace.h"
#include "config.h"
#include "kernel.h"

static U64 s_wallTick=0;
static Time_S s_wallTime;
static char s_time_string[12]={0};

void rtc_gettime(Time_S* ptime)
{
    U64 diff = CoGetOSTime()-s_wallTick;
    U64 seconds = diff/SYSTEM_TICK_HZ;    
    ptime->m_year = s_wallTime.m_year;
    ptime->m_month = s_wallTime.m_month;
    ptime->m_date = s_wallTime.m_date;
    ptime->m_weekday = s_wallTime.m_weekday;
    ptime->m_hour = s_wallTime.m_hour+seconds/3600;
    if(ptime->m_hour>23)
    {
        ptime->m_hour = ptime->m_hour%24;
    }
    seconds = seconds%3600;
    ptime->m_minute = s_wallTime.m_minute+seconds/60;
    if(ptime->m_minute>59)
    {
        ptime->m_minute = ptime->m_minute%60;
    }
    seconds = seconds%60;
    ptime->m_second =s_wallTime.m_second+seconds;
    if(ptime->m_second>59)
    {
        ptime->m_second = ptime->m_second%60;
    }
}
void rtc_settime(Time_S* ptime)
{
    s_wallTime.m_year = ptime->m_year;
    s_wallTime.m_month = ptime->m_month;
    s_wallTime.m_date = ptime->m_date;
    s_wallTime.m_weekday = ptime->m_weekday;
    s_wallTime.m_hour = ptime->m_hour;
    s_wallTime.m_minute = ptime->m_minute;
    s_wallTime.m_second = ptime->m_second;
    s_wallTick = CoGetOSTime();
}

char* rtc_strtime(void)
{
    Time_S time={0};
    rtc_gettime(&time);
    sprintf(s_time_string,"%02d:%02d:%02d",
            time.m_hour,time.m_minute,time.m_second);
    return s_time_string;
}

char* rtc_strdate(void)
{
    Time_S time={0};
    rtc_gettime(&time);
    sprintf(s_time_string,"%4d-%02d-%02d",
            time.m_hour,time.m_minute,time.m_second);
    return s_time_string;
}

void rtc_reset(void)
{
    Time_S time;
    time.m_year = 2018;
    time.m_month = 1;
    time.m_date = 1;
    time.m_weekday = 0;
    time.m_hour = 0;
    time.m_minute = 0;
    time.m_second = 0;
    rtc_settime(&time);
}

