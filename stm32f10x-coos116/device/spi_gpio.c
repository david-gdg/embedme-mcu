/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "board.h"
#include "gpio.h"
#include "spi.h"
#include "kernel.h"
#include "mymutex.h"

static SpiBus_S s_spiBus[SPI_BUS_MAX]={
  /* MOSI  MISO  SCK   currentDev  mutex  initFlag */
    {PA(7),PA(6),PA(5),SPI_DEV_NA  ,-1    ,0},  /* SPI_BUS_0 */
    {0    ,0    ,0    ,SPI_DEV_NA  ,-1    ,0},  /* SPI_BUS_1 */
    {0    ,0    ,0    ,SPI_DEV_NA  ,-1    ,0}   /* SPI_BUS_2 */
};

static SpiDev_S s_spiDev[SPI_DEV_MAX]={
    {SPI_DEV_PGA2311_1,SPI_BUS_0,0,PA(4)},
    {SPI_DEV_PGA2311_2,SPI_BUS_1,0,PA(3)}
};

static bool spi_gpio_init(uint8 spi_bus)
{
    switch(spi_bus){
    case SPI_BUS_0:
    case SPI_BUS_1:
    case SPI_BUS_2:
    {
        if(s_spiBus[spi_bus].m_initFlag!=0)
        {
            return true;
        }
        if(s_spiBus[spi_bus].m_gpioMOSI!=0)
        {
            GPIO_INIT(s_spiBus[spi_bus].m_gpioMOSI,GPIO_MODE_OUT_PP,GPIO_Speed_50MHz);
        }
        if(s_spiBus[spi_bus].m_gpioMISO!=0)
        {
            GPIO_INIT(s_spiBus[spi_bus].m_gpioMISO,GPIO_MODE_IN_FLOATING,GPIO_Speed_50MHz);
        }
        if(s_spiBus[spi_bus].m_gpioSCK!=0)
        {
            GPIO_INIT(s_spiBus[spi_bus].m_gpioSCK,GPIO_MODE_OUT_PP,GPIO_Speed_50MHz);
        }
        break;
    }
    default:
        return false;
    }
    s_spiBus[spi_bus].m_mutex = mutex_init();
    if(s_spiBus[spi_bus].m_mutex<0)
    {
        return false;
    }
    s_spiBus[spi_bus].m_initFlag = 1;
    return true;
}

static void spi_gpio_set_dir(uint8 spi_bus, uint8 dir)
{   
    if(dir==DIR_OUT)
    {
		    GPIO_INIT(s_spiBus[spi_bus].m_gpioMOSI, GPIO_MODE_OUT_PP, GPIO_SPEED_50MHZ);
		}
    else
    {
		    GPIO_INIT(s_spiBus[spi_bus].m_gpioMISO, GPIO_MODE_IN_FLOATING, GPIO_SPEED_50MHZ);
		}
}

static void spi_gpio_set_databit(uint8 spi_bus,uint8 val)
{
    GPIO_SET_VAL(s_spiBus[spi_bus].m_gpioMOSI, val);
}

static uint8 spi_gpio_get_databit(uint8 spi_bus)
{
    return GPIO_GET_VAL(s_spiBus[spi_bus].m_gpioMISO);
}

static void spi_gpio_set_sck(uint8 spi_bus,uint8 val)
{
    GPIO_SET_VAL(s_spiBus[spi_bus].m_gpioSCK, val);
}

static bool spi_gpio_set_dev(uint8 spi_dev, bool enable)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(s_spiBus[spi_bus].m_initFlag)
    {
        GPIO_INIT(s_spiDev[spi_dev].m_gpioCS,GPIO_MODE_OUT_PP,GPIO_Speed_50MHz);
        if(enable)
        {
            GPIO_SET_VAL(s_spiDev[spi_dev].m_gpioCS, s_spiDev[spi_dev].m_enCS);
        }
        else
        {
            GPIO_SET_VAL(s_spiDev[spi_dev].m_gpioCS, !(s_spiDev[spi_dev].m_enCS));
        }
        return true;
    }
    return false;
}

bool spi_gpio_open(uint8 spi_dev)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(!spi_gpio_init(spi_bus))
    {
        return false;
    }
    mutex_lock(s_spiBus[spi_bus].m_mutex);
    if(!spi_gpio_set_dev(spi_dev, true))
    {
        return false;
    }
    s_spiBus[spi_bus].m_currDev = spi_dev;
    return true;
}

bool spi_gpio_close(uint8 spi_dev)
{
    uint8 spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return false;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    if(s_spiBus[spi_bus].m_currDev!=spi_dev)
    {
        return false;
    }
    if (spi_gpio_set_dev(spi_dev, false))
    {
        return false;
    }
    s_spiBus[spi_bus].m_currDev = -1;
    mutex_unlock(s_spiBus[spi_bus].m_mutex);
    return true;
}

uint8 spi_gpio_write(uint8 spi_dev,uint8 byte)
{
    uint8 i,spi_bus;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return 0;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    OS_ENTER_CRITICAL();
    spi_gpio_set_dir(spi_bus,DIR_OUT);
    /* MSB传输(高位在前,低位在后) */
    for (i = 0; i < 8; i++)
    {
        if ((byte & 0x80) == 0x80)
        {
            spi_gpio_set_databit(spi_bus,GPIO_HIGH);
        }
        else
        {
            spi_gpio_set_databit(spi_bus,GPIO_LOW);
        }
        udelay(5);
        spi_gpio_set_sck(spi_bus,1);
        udelay(5);
        spi_gpio_set_sck(spi_bus,0);
        udelay(5);
        byte <<= 1;
    }
    OS_EXIT_CRITICAL();
    return 1;
}

uint8 spi_gpio_read(uint8 spi_dev, uint8* pByte)
{
    uint8 i,spi_bus,byte;
    byte = 0;
    if(spi_dev>=SPI_DEV_MAX)
    {
        return 0;
    }
    spi_bus = s_spiDev[spi_dev].m_spiBus;
    OS_ENTER_CRITICAL();
    spi_gpio_set_dir(spi_bus,DIR_IN);
    for (i = 0; i < 8; i++)
    {
        spi_gpio_set_sck(spi_bus,1);
        udelay(5);
        byte <<= 1;
        if(spi_gpio_get_databit(spi_bus)==GPIO_HIGH)
        {
            byte |= 1;
        }
        spi_gpio_set_sck(spi_bus,0);
        udelay(5);
    }
    OS_EXIT_CRITICAL();
    *pByte = byte;
    return 1;
}

/* 有些器件使用CS来判断是否读写完毕 */
void spi_gpio_xfer_start(uint8 spi_dev)
{
    spi_gpio_set_dev(spi_dev,true);
}

/* 有些器件使用CS来判断是否读写完毕 */
void spi_gpio_xfer_stop(uint8 spi_dev)
{
    spi_gpio_set_dev(spi_dev,false);
}

