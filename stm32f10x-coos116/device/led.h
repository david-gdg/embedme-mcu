/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __LED_H__
#define __LED_H__
#include "basetype.h"
#include "gpio.h"
typedef enum{
    LED_L0=0,   /* 00: LED1 */
    LED_L1,     /* 01: LED2 */
    LED_L2,     /* 02: LED3 */
    LED_L3,     /* 03: LED4 */
    LED_MAX,
}LED_ID_E;

typedef enum{
    LED_STATE_OFF=0,
    LED_STATE_ON,
    LED_STATE_BLINK_1HZ,
    LED_STATE_BLINK_2HZ,
    LED_STATE_BLINK_5HZ,
    LED_STATE_NA=0x1F
}LED_STATE_E;

bool led_init(uint8 ledID,GpioInfo_S gpioInfo);
void led_set_state(uint8 ledID,uint8 ledState);
uint8 led_get_state(uint8 ledID);
#endif
