/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "trace.h"
#include "system.h"
#include "mytimer.h"
#include "rtc.h"
#include "kernel.h"

#define TIMER_COUNT_MAX   16
#define TIMER_TICKS_MS    10     /* 定时器时间片(10ms) */
/*-------------------------------------------------------------------------
* 如果系统定时器的时钟片周期我们设置为10ms的定时精度,
* 则最长定时时间则为2^30*10ms=1073741824ms=1073741S
-------------------------------------------------------------------------*/
typedef struct{
    uint32 m_used:1;   /* 0:未使用,1:已使用 */
    uint32 m_start:1;  /* 0:停止,1:开始 */
    uint32 m_reload:30;/* 定时时间片数 */
    uint32 m_type:2;   /* 定时器类型 */
    uint32 m_ticks:30; /* 剩余时间片 */
    SEL_ontimer m_ontimer;
}TimerInfo_S;
static TimerInfo_S s_timerpool[TIMER_COUNT_MAX]={0};

/*******************************************************
 * timer_create:
 * 创建定时器
 * timerType 定时器类型 TIMER_TYPE_E
 * ms_abs    定时时间(单位为ms,最大为2^30),如果是绝对时间
             定时,此参数使用宏TIMER_ABS_TIME生成.
 * ontimer   定时器回调函数
 ******************************************************/
sint8 timer_create(uint8 timerType,uint32 ms_abs,SEL_ontimer ontimer)
{
    uint8 i;
    for (i=0;i<TIMER_COUNT_MAX; i++)
    {
        if (s_timerpool[i].m_used==0)
        {
            s_timerpool[i].m_used=1;
            s_timerpool[i].m_start=0;
            s_timerpool[i].m_type = timerType;
            s_timerpool[i].m_ontimer = ontimer;
            if(timerType!=TIMER_TYPE_ABSTIME)
            {
                s_timerpool[i].m_reload = (ms_abs+10)/(TIMER_TICKS_MS);
            }
            else
            {
                s_timerpool[i].m_reload = ms_abs;
            }
            return i;
        }
    }
    return -1;
}
/*******************************************************
 * timer_detroy:
 * 销毁定时器
 * timerID 定时器ID
 ******************************************************/
void timer_detroy(sint8 timerID)
{
     if (timerID>=0 &&
         timerID<TIMER_COUNT_MAX && 
         s_timerpool[timerID].m_used==1)
     {
        s_timerpool[timerID].m_used=0;
     }
}

/*******************************************************
 * timer_start:
 * 启动定时器
 * timerID 定时器ID
 ******************************************************/
bool timer_start(sint8 timerID)
{
    if (timerID>=0 &&
        timerID<TIMER_COUNT_MAX &&
        (s_timerpool[timerID].m_used==1) &&
        (s_timerpool[timerID].m_ontimer!=0))
    {
        s_timerpool[timerID].m_start=1;
        s_timerpool[timerID].m_ticks = s_timerpool[timerID].m_reload;
        return true;
    }
    return false;
}
/*******************************************************
 * timer_stop:
 * 停止定时器
 * timerID 定时器ID
 ******************************************************/
void timer_stop(sint8 timerID)
{
    if (timerID>=0 &&
        timerID<TIMER_COUNT_MAX &&
        (s_timerpool[timerID].m_used==1))
    {
        s_timerpool[timerID].m_start=0;
    }
}

/* 定时器任务 
 * 我们使用msleep在任务内进行延时,以此作为时间基准.
 * 这种实现方式实现的定时不会很准,因为还要考虑任务切换所花费的时间.
 */
void task_timer_do_tick(void* pData)
{
    uint8 i=0;
    uint32 times=0;
    while(1)
    {
        times++;
        for(i=0;i<TIMER_COUNT_MAX;i++)
        {
            if(s_timerpool[i].m_used==1 && 
               s_timerpool[i].m_start==1)
            {
                if(s_timerpool[i].m_type!=TIMER_TYPE_ABSTIME)
                {
                    if(s_timerpool[i].m_ticks==0)
                    {
                        if (s_timerpool[i].m_ontimer!=NULL)
                        {
                           s_timerpool[i].m_ontimer(i); 
                        }
                        if(TIMER_TYPE_ONESHOT==s_timerpool[i].m_type)
                        {
                            s_timerpool[i].m_used=0;
                        }
                        else
                        {
                            s_timerpool[i].m_ticks = s_timerpool[i].m_reload;
                        }
                    }
                    s_timerpool[i].m_ticks--;
                }
                else if(times%50==0)/* 500ms检查一次绝对时间 */
                {
                    uint16 time;
                    Time_S rtcTime={0};
                    rtc_gettime(&rtcTime);
                    time = TIMER_ABS_TIME(rtcTime.m_hour, rtcTime.m_minute, rtcTime.m_second);
                    if (s_timerpool[i].m_reload==time && s_timerpool[i].m_ontimer!=NULL)
                    {
                        s_timerpool[i].m_ontimer(i);
                    }
                }
            }
        }
        msleep(TIMER_TICKS_MS);
    }
}


