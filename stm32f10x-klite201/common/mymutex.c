/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "mymutex.h"
#include "kernel.h"

/* 最大可使用的互斥锁个数 */
#define MUTEX_COUNT_MAX  32

static kmutex_t s_mutexPool[MUTEX_COUNT_MAX]={NULL};

sint8 mutex_init(void)
{
    sint8 i=0;
    for(i=0; i<MUTEX_COUNT_MAX; i++)
    {
        if(s_mutexPool[i]==NULL)
        {
            s_mutexPool[i] = kmutex_create();
            if(s_mutexPool[i]!=NULL)
            {
                return i;
            }
        }
    }
    return (-1);
}

void mutex_lock(sint8 mutexID)
{
    if (mutexID<0 || 
        mutexID>=MUTEX_COUNT_MAX ||
        s_mutexPool[mutexID]==NULL)
    {
        return;
    }
    kmutex_lock(s_mutexPool[mutexID]);
}

void mutex_unlock(sint8 mutexID)
{
    if (mutexID<0 || 
        mutexID>=MUTEX_COUNT_MAX ||
        s_mutexPool[mutexID]==NULL)
    {
         return;
    }
    kmutex_unlock(s_mutexPool[mutexID]);
}

