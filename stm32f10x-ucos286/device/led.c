/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#include "led.h"
#include "gpio.h"
#include "mytimer.h"
#include "trace.h"
#include "board.h"

#define LED_TIMER_TIMEOUT    50   /* 时间片为50ms */
#define BLINKFREQ_1HZ_TICKS  10   /* 1HZ,周期为1000ms,则500ms反相一次 */
#define BLINKFREQ_2HZ_TICKS  5    /* 2HZ,周期为500ms,则250ms反相一次 */
#define BLINKFREQ_5HZ_TICKS  2    /* 5HZ,周期为200ms,则100ms反相一次 */

typedef struct{
    uint32 m_gpio;
    uint8 m_enVal:1;
    uint8 m_blink:1;
    uint8 m_inited:1;
    uint8 m_state:5;
    sint8 m_blinkTime;
    sint8 m_timeCount;
}LedInfo_S;

static LedInfo_S s_ledInfo[LED_MAX]={0};

static void led_set_onoff(uint8 ledID,uint8 ledState)
{
    if(s_ledInfo[ledID].m_state == ledState)
    {
        return;
    }
    //TRACE_DBG("set led(%d),state(%d), gpio[%d:%d] en(%d)",ledID,ledState,s_ledInfo[ledID].m_group,s_ledInfo[ledID].m_pin,s_ledInfo[ledID].m_enVal);
    if(ledState==LED_STATE_ON) {
        if(s_ledInfo[ledID].m_enVal==GPIO_HIGH) {
            GPIO_SET_VAL(s_ledInfo[ledID].m_gpio,GPIO_HIGH);
        } else {
            GPIO_SET_VAL(s_ledInfo[ledID].m_gpio,GPIO_LOW);
        }     
    } else {
        if(s_ledInfo[ledID].m_enVal==GPIO_HIGH) {
            GPIO_SET_VAL(s_ledInfo[ledID].m_gpio, GPIO_LOW);
        } else {
            GPIO_SET_VAL(s_ledInfo[ledID].m_gpio, GPIO_HIGH);
        }
    }
    s_ledInfo[ledID].m_state = ledState;
}

static void led_set_blink(uint8 ledID,uint8 ledState)
{
    if(s_ledInfo[ledID].m_state==ledState)
    {
        return;
    }
    switch(ledState){
    case LED_STATE_BLINK_2HZ:
        s_ledInfo[ledID].m_blinkTime = BLINKFREQ_2HZ_TICKS;
        break;
    case LED_STATE_BLINK_5HZ:
        s_ledInfo[ledID].m_blinkTime = BLINKFREQ_5HZ_TICKS;
        break;
    default:
        s_ledInfo[ledID].m_blinkTime = BLINKFREQ_1HZ_TICKS;
        break;
    }
    s_ledInfo[ledID].m_timeCount = s_ledInfo[ledID].m_blinkTime;
}

static void on_led_check_timer(sint8 timeID)
{
    uint8 i;
    for(i=0; i<LED_MAX; i++)
    {
        if(s_ledInfo[i].m_inited==1 &&
           s_ledInfo[i].m_blink==1)
        {
            s_ledInfo[i].m_timeCount--;
            if(s_ledInfo[i].m_timeCount<=0)
            {
                //设置LED反相,并重新加载timeCount
                if(s_ledInfo[i].m_state==LED_STATE_ON)
                    led_set_onoff(i, LED_STATE_OFF);
                else
                    led_set_onoff(i, LED_STATE_ON);
                s_ledInfo[i].m_timeCount = s_ledInfo[i].m_blinkTime;
            }
        }
    }
}

/*******************************************************
 * led_init:
 * LED初始化(默认为关闭状态)
 ******************************************************/
static bool s_ledInitFlag = false;
bool led_init(uint8 ledID,GpioInfo_S gpioInfo)
{
    s_ledInfo[ledID].m_gpio = gpioInfo.m_gpio;
    s_ledInfo[ledID].m_enVal = gpioInfo.m_enVal;
    s_ledInfo[ledID].m_inited = 1;
    s_ledInfo[ledID].m_blink=0;
    led_set_onoff(ledID, LED_STATE_OFF);
    if(!s_ledInitFlag)
    {
        sint8 timerID;
        s_ledInitFlag = true;
        timerID = timer_create(TIMER_TYPE_PERIODIC, LED_TIMER_TIMEOUT,on_led_check_timer);
        if(timerID<0)
        {
            TRACE_ERR("led manager timer create error.");
            return false;
        }

        if(!timer_start(timerID))
        {
            TRACE_ERR("led manager timer start error.");
            return false;
        }
    }
    return true;
}


bool led_start(void)
{
    sint8 timerID = timer_create(TIMER_TYPE_PERIODIC, LED_TIMER_TIMEOUT,on_led_check_timer);
    if(timerID<0)
    {
        TRACE_ERR("led manager timer create error.");
        return false;
    }

    if(!timer_start(timerID))
    {
        TRACE_ERR("led manager timer start error.");
        return false;
    }
    return true;
}
/*******************************************************
 * led_set_state:
 * 设置LED状态(开或者关)
 ******************************************************/
void led_set_state(uint8 ledID,uint8 ledState)
{
    if(ledID<LED_MAX && s_ledInfo[ledID].m_inited==1)
    {
        switch(ledState){
        case LED_STATE_OFF:
        case LED_STATE_ON:
            s_ledInfo[ledID].m_blink=0;
            led_set_onoff(ledID, ledState);
            break;
        case LED_STATE_BLINK_1HZ:
        case LED_STATE_BLINK_2HZ:
        case LED_STATE_BLINK_5HZ:
            s_ledInfo[ledID].m_blink=1;
            led_set_blink(ledID, ledState);
            break;   
        } 
    }
}

uint8 led_get_state(uint8 ledID)
{
    if(ledID<LED_MAX)
    {
        return s_ledInfo[ledID].m_state;
    }
    return LED_STATE_NA;
}



