/******************************************************************************
 * This file is part of embedme-mcu.
 *
 * embedme-mcu is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * embedme-mcu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with embedme-mcu.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Project: embedme-mcu
 * Author : FergusZeng
 * Email  : cblock@126.com
 * git	  : https://git.oschina.net/cblock/embedme-mcu
 * Copyright 2013~2019 @ ShenZhen ,China
*******************************************************************************/
#ifndef __MEM_POOL_H__
#define __MEM_POOL_H__

#include "basetype.h"

typedef enum{
   MEM_POOL_4B,     /* used by: InputEventMsg_S */
   MEM_POOL_24B,    /* used by: SdsStatusMsg_S */
   MEM_POOL_160B,   /* used by: SdsTextMsg_S */
   MEM_POOL_280B,   /* used by: SdsLongMsg_S */
   MEM_POOL_MAX
}MEM_POOL_E;

bool mempool_init(void);
void* mempool_get_mem(uint8 mempool);
bool mempool_put_mem(uint8 mempool,void* mem);

#endif

